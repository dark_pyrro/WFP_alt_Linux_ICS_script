#!/bin/bash

# Using this script will probably ruin DNS on the Linux box itself (requires some fiddling to restore, or simply restart the machine)
# The script might need some tweaking depending on the distro used
# Tested on a couple of Debian based distros such as GalliumOS, Linux Lite, Ubuntu, Raspberry Pi OS Lite, Kali Linux, etc. (as of March 2023)

# One (1) command line argument should be passed to the script and that is the interface on the Linux machine that provides access to the internet

clear

if [ "$EUID" -ne 0 ]; then
    echo ""
    echo "This WiFi Pineapple Connection script requires root permissions. Exiting..."
    echo ""
    exit 1
fi

if [ $# -ne 1 ]
  then
    echo ""
    echo "One (1) command line argument should always be used with this script and that is the"
    echo "interface on the Linux machine that provides access to the internet, for example: wlp1s0"
    echo "Exiting..."
    echo ""
    exit 1
fi

# The interface that enables internet/network access for the Linux box
spineapplewan=$1
IF_CHK=$(ip link show | grep -w $spineapplewan | cut -d ' ' -f2 | sed 's/.$//' | wc -l)

if [ $IF_CHK -ne 1 ]
  then
    echo ""
    echo "The interface provided as a command line argument doesn't seem to exist on this computer. Exiting..."
    echo ""
    exit 1
fi

# The interface on the Linux box that represents the Pineapple USB-C connection (note; the Pinepple needs to be connected to the Linux machine using USB-C and running fully)
# This assumes that the MAC address of the Pineapple haven't been changed in some way 
spineapplelan=$(ip addr | grep '00:[cC]0:[cC][aA]\|00:13:37' -B1 | awk {'print $2'} | head -1 | grep 'eth\|en' | sed 's/.$//')
# The IP address of the gateway used by the Linux box
spineapplegw=$(ip -4 r | grep default | grep $spineapplewan | cut -d ' ' -f3)

# Leave unchanged
spineapplehostip=172.16.42.42
spineapplenmask=255.255.255.0
spineappleip=172.16.42.1
spineapplenet=172.16.42.0/24

ip link set dev $spineapplelan down
ip addr add $spineapplehostip/$spineapplenmask dev $spineapplelan
ip link set dev $spineapplelan up
until ping $spineappleip -c1 -w1 >/dev/null
do
	ip link set dev $spineapplelan up
	sleep 1
done
ip addr add $spineapplehostip/$spineapplenmask dev $spineapplelan
ip link set dev $spineapplelan up
echo '1' > /proc/sys/net/ipv4/ip_forward
iptables -X
iptables -F
iptables -A FORWARD -i $spineapplewan -o $spineapplelan -s $spineapplenet -m state --state NEW -j ACCEPT
iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A POSTROUTING -t nat -j MASQUERADE
ip route del default
ip route add default via $spineapplegw dev $spineapplewan
