Just an alternative script to set up ICS on Linux machines for the Hak5 WiFi Pineapple Mark VII  

Based on the script `wp7.sh` made by Hak5  
https://downloads.hak5.org/pineapple/mk7  
https://docs.hak5.org/wifi-pineapple/faq/establishing-an-internet-connection/configuring-ics-on-linux  

Read the script comments for more information about how it works  

Use at your own risk...  